﻿Imports System.Configuration
Imports Microsoft.Office
Imports Microsoft.Office.Interop.Excel

Public Class clsEmail


    Public Sub SendEmail(sEmailTo As String, sEmailCc As String, sSubject As String, sBody As String, _
                     sFilename As String, sDisplayname As String)

        Dim oApp As Interop.Outlook.Application
        oApp = New Interop.Outlook.Application

        Dim oMsg As Interop.Outlook._MailItem
        oMsg = oApp.CreateItem(Interop.Outlook.OlItemType.olMailItem)

        oMsg.Subject = sSubject
        oMsg.Body = sBody

        'oMsg.
        oMsg.To = sEmailTo
        oMsg.CC = sEmailCc


        Dim strS As String = sFilename
        Dim strN As String = sDisplayname
        If sFilename <> "" Then
            Dim sBodyLen As Integer = Int(sBody.Length)
            ' Dim oAttach As Interop.Outlook.Attachment

            Dim oAttachs As Interop.Outlook.Attachments = oMsg.Attachments
            Dim myAttachments As Interop.Outlook.Attachments

            myAttachments = oMsg.Attachments
            myAttachments.Add(sFilename, Interop.Outlook.OlAttachmentType.olByValue, 1, sFilename)
        End If

        oMsg.Send()

    End Sub
End Class
