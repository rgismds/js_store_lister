﻿Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports Microsoft.Office.Interop.Excel
Imports System.Data.SqlClient



Public Class Form1

    Dim bAutoMode As Boolean


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim currDate As Date = DateTime.Now

        CSVTextbox.Text = "\\eu-fps\Client_Data\Sainsburys\Dashboard\DAILY_PCVAR"
        TempTextBox.Text = "c:\JS_Temp"
        If currDate.DayOfWeek = DayOfWeek.Monday Then
            WCDATE.MaxDate = DateTime.Today.AddDays(-8)
        Else
            If currDate.DayOfWeek = DayOfWeek.Tuesday Then
                WCDATE.MaxDate = DateTime.Today.AddDays(-9)
            End If
            If currDate.DayOfWeek = DayOfWeek.Wednesday Then
                WCDATE.MaxDate = DateTime.Today.AddDays(-10)
            End If
            If currDate.DayOfWeek = DayOfWeek.Thursday Then
                WCDATE.MaxDate = DateTime.Today.AddDays(-11)
            End If
            If currDate.DayOfWeek = DayOfWeek.Friday Then
                WCDATE.MaxDate = DateTime.Today.AddDays(-12)
            End If
        End If
        Dim sParams() As String
        sParams = Environment.GetCommandLineArgs()
        If sParams.Count > 1 Then
            If sParams(1).ToUpper() = "AUTO" Then
                bAutoMode = True
                Me.Visible = False
                PopulateStoreList()
                CreateStoreFile()
            End If
        End If

        If bAutoMode Then
            System.Windows.Forms.Application.Exit()
        End If


    End Sub

    Private Sub PopulateStoreList()
        Dim str As New List(Of String)
        Dim MyReader As StreamReader
        Dim nLine As Integer
        Dim sCurrLine As String


        'check temp folder exist if so delete it
        Me.Cursor = Cursors.WaitCursor

        If Directory.Exists(TempTextBox.Text) Then
            My.Computer.FileSystem.DeleteDirectory(TempTextBox.Text,
                FileIO.DeleteDirectoryOption.DeleteAllContents)
        End If

        'Recreate the Temp Folder
        Directory.CreateDirectory(TempTextBox.Text)


        Dim Folder As New IO.DirectoryInfo(CSVTextbox.Text)
        For Each File As IO.FileInfo In Folder.GetFiles("*.csv", IO.SearchOption.TopDirectoryOnly) _
            .Where(Function(x) x.LastWriteTime >= (WCDATE.Value) AndAlso x.LastWriteTime <= (WCDATE.Value.AddDays(7)))
            toolStatusText.Text = "Copying " & File.Name.ToString
            Dim CF As New IO.FileStream(File.FullName, IO.FileMode.Open)
            Dim CT As New IO.FileStream(Path.Combine(TempTextBox.Text, File.Name), IO.FileMode.Create)
            Dim len As Long = CF.Length - 1
            Dim buffer(1024) As Byte
            Dim byteCFead As Integer
            While CF.Position < len
                byteCFead = (CF.Read(buffer, 0, 1024))
                CT.Write(buffer, 0, byteCFead)
                ProgressBar1.Value = CInt(CF.Position / len * 99)
                System.Windows.Forms.Application.DoEvents()
            End While
            CT.Flush()
            CT.Close()
            CF.Close()
            ' FileCopy(File.FullName, Path.Combine(TempTextBox.Text, File.Name))
            ListBox1.Items.Add(File.Name)
            System.Windows.Forms.Application.DoEvents()
        Next

        If ListBox1.Items.Count <> 7 Then
            MsgBox("File Count Wrong File's Found: " & ListBox1.Items.Count)
        End If

        ProgressBar1.Value = 0


        For Each oItem In ListBox1.Items


            ProgressBar1.Value = ProgressBar1.Value + (100 \ ListBox1.Items.Count)
            toolStatusText.Text = "Reading File: " & oItem.ToString

            If My.Computer.FileSystem.GetFileInfo(TempTextBox.Text & "/" & oItem).Length > "155" Then

                Dim File_Date = Convert.ToDateTime(System.IO.File.GetLastWriteTime(CSVTextbox.Text & "/" & oItem)).AddDays(-1).ToString(("dd/MM/yyyy"))

                nLine = 0

                Try
                    ' JR - 13/03/2017
                    ' Changed MyReader to be a StreamReader rather than a TextFieldParser as the TextFieldParser object
                    ' was generating parse errors on some lines that included double characters

                    MyReader = New StreamReader(TempTextBox.Text & "/" & oItem.ToString)
                    ' MyReader = New TextFieldParser(TempTextBox.Text & "/" & oItem.ToString)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                Dim currentRow As String()
                ' Read the first line and do nothing with it
                If Not MyReader.EndOfStream Then
                    sCurrLine = MyReader.ReadLine()
                    currentRow = sCurrLine.Split(",")
                End If
                nLine += 1
                While Not MyReader.EndOfStream
                    Try
                        ' Read again the file
                        sCurrLine = MyReader.ReadLine()
                        currentRow = sCurrLine.Split(",")
                        'currentRow = MyReader.ReadFields()
                        Dim line_add = (currentRow(0) & "," & File_Date)
                        If Not str.Contains(line_add) Then
                            DataGridView1.Rows.Add(currentRow(0), File_Date)
                            str.Add(line_add)
                        End If
                        nLine += 1
                    Catch ex As Microsoft.VisualBasic.FileIO.MalformedLineException
                        MsgBox("Line " & ex.Message & "is not valid and will be skipped.")
                    Catch ex As Exception
                        ex = ex
                    End Try
                End While
                MyReader.Close()
                MyReader.Dispose()

            End If

        Next


        ProgressBar1.Value = 100
        toolStatusText.Text = "A total of " + (DataGridView1.RowCount - 1).ToString() + " stores were found"
        StatusStrip1.Refresh()

        btnCreateStoreFile.Enabled = True
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        PopulateStoreList()
    End Sub


    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub btnCreateStoreFile_Click(sender As Object, e As EventArgs) Handles btnCreateStoreFile.Click
        Call CreateStoreFile()
    End Sub
    Private mConn As String = ""

    Private Function DecryptString(ByVal encrString As String) As String
        Try
            Dim b As Byte() = Convert.FromBase64String(encrString)
            Dim decryptedConnectionString As String = System.Text.ASCIIEncoding.ASCII.GetString(b)
            Return decryptedConnectionString
        Catch
            Throw
        End Try
    End Function
    Private Function LoadSavedConnectionDetailsFromRegistry(ByRef mConn As String) As Boolean
        Dim bOK As Boolean = False

        Try
            Dim sKeyName As String = "Dashboard_Config"
            Dim sSectionName As String = "Settings"

            mConn = DecryptString(GetSetting(sKeyName, sSectionName, "ConnString", ""))

            If mConn.Length > 0 AndAlso CreateConnection(mConn) Then
                bOK = True
            End If

        Catch ex As Exception
        Finally
            LoadSavedConnectionDetailsFromRegistry = bOK
        End Try

    End Function
    Private Function CreateConnection(ByVal sConnString As String) As Boolean
        Dim bOk As Boolean = False
        Try

            Dim oConn As IDbConnection = Nothing

            oConn = New SqlConnection

            oConn.ConnectionString = sConnString
            oConn.Open()

            bOk = True

        Catch ex As Exception

        Finally
            CreateConnection = bOk
        End Try
    End Function

    Private Sub CreateStoreFile()

        Dim xlApp As Microsoft.Office.Interop.Excel.Application
        Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
        Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim range As Microsoft.Office.Interop.Excel.Range
        Dim sFile As String
        Dim nRow, nCol As Integer
        Dim sStoreType, sStoreName As String
        Dim sStoreListFolder As String
        Dim sDate As String

        Dim sOutputStoreFile As String
        Dim xoCmd As SqlCommand
        Dim sConnStr As String = ""
        Dim DBConn As SqlConnection

        Cursor = Cursors.WaitCursor
        sStoreListFolder = "\\eu-fps.eu.ad.rgis.com\Private\Division\Schedules\Sainsbury's\Wkly Store Upload List\"
        sFile = sStoreListFolder + "Weekly_storelist_blank.xlsx"
        sOutputStoreFile = sStoreListFolder + "wc " + WCDATE.Value.Day.ToString("D2") + "." + WCDATE.Value.Month.ToString("D2") + "." + WCDATE.Value.Year.ToString("D2") + ".xlsx"

        xlApp = New Microsoft.Office.Interop.Excel.Application()
        xlWorkBook = xlApp.Workbooks.Open(sFile)
        xlWorkSheet = xlWorkBook.Worksheets(1)
        range = xlWorkSheet.UsedRange
        nRow = 2
        nCol = 1
        Call LoadSavedConnectionDetailsFromRegistry(sConnStr)
        DBConn = New SqlConnection(sConnStr)
        DBConn.Open()
        xlWorkSheet.Columns(3).NumberFormat = "dd/mm/yyyy"
        For Each r In DataGridView1.Rows
            If (Not IsNothing(r.Cells(0).Value)) Then


                xoCmd = New SqlCommand
                xoCmd.Connection = DBConn
                xoCmd.CommandType = CommandType.StoredProcedure

                Try
                    ' Dim xdReportDate As Date
                    xoCmd.CommandText = "p_GetStoreNameAndType"
                    xoCmd.Parameters.Add("Store_Number", SqlDbType.Int).Value = r.Cells(0).Value.ToString()
                    xoCmd.Parameters.Add("Store_Type", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output
                    xoCmd.Parameters.Add("Store_Name", SqlDbType.NVarChar, 255).Direction = ParameterDirection.Output
                    xoCmd.ExecuteNonQuery()

                    sStoreType = xoCmd.Parameters("Store_Type").Value.ToString
                    sStoreName = xoCmd.Parameters("Store_Name").Value.ToString
                    ' Format
                    xlWorkSheet.Cells(nRow, nCol) = sStoreType
                    ' Store Number
                    xlWorkSheet.Cells(nRow, nCol + 1) = r.Cells(0).Value.ToString()
                    ' Store Name
                    xlWorkSheet.Cells(nRow, nCol + 2) = sStoreName
                    ' Date
                    sDate = r.Cells(1).Value.ToString()
                    xlWorkSheet.Cells(nRow, nCol + 3) = sDate


                Catch ex As Exception
                    Call MessageBox.Show("Error when processing data: " & ex.Message)

                End Try

            End If
            nRow = nRow + 1
        Next
        xlApp.DisplayAlerts = False

        xlWorkBook.SaveAs(sOutputStoreFile)
        If bAutoMode Then
            Dim cEmail As clsEmail
            Dim sEmailTo, sEmailCc, sBody, sSubject As String

            cEmail = New clsEmail()
            sBody = "Hello," + vbCrLf + vbCrLf
            sBody += "Please note we have determined there are " + (DataGridView1.RowCount - 1).ToString() + " stores due to be imported for the week commencing "
            sBody += WCDATE.Value.Day.ToString("D2") + "." + WCDATE.Value.Month.ToString("D2") + "." + WCDATE.Value.Year.ToString("D2") + vbCrLf + vbCrLf
            sBody += "We shall wait for you to confirm this is correct before proceeding to import the data." + vbCrLf + vbCrLf
            sBody += "Regards" + vbCrLf + "Mapping & Data Services Team"
            sEmailTo = "Robert.Holden@sainsburys.co.uk"
            sEmailCc = "Magdalena.Malanowicz@sainsburys.co.uk;GRichards@rgis.com;VMolnar@RGIS.com;mds@rgis.com"
            sSubject = "Sainsburys import " + WCDATE.Value.Day.ToString("D2") + "." + WCDATE.Value.Month.ToString("D2") + "." + WCDATE.Value.Year.ToString("D2")
            ' If running in automatic mode then send store file immediately to Sainsburys
            cEmail.SendEmail(sEmailTo, sEmailCc, sSubject, sBody, sOutputStoreFile, "")

        End If
        xlWorkBook.Close()
        xlApp.Quit()
        DBConn.Close()
        Cursor = Cursors.Default
        toolStatusText.Text = "File saved to " + sOutputStoreFile
    End Sub
End Class



